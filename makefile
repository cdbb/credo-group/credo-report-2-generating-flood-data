SOURCE_FILES := $(wildcard ./*/*.py) Dockerfile
.PHONY: build clean local

dafni_models/hipims-model.tar.gz: $(SOURCE_FILES)
	docker build --target build_hipims .
	docker build --target build_pypims .
	docker build -t hipims-model:to-upload .
	docker save hipims-model:to-upload | gzip > $@

build: dafni_models/hipims-model.tar.gz

clean:
	rm -f dafni_models/hipims*

local:
	docker build --target build_hipims .
	docker build --target build_pypims .
	docker build -t hipims-model .
