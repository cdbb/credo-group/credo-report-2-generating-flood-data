import json
from os import getenv
from os.path import join
from pathlib import Path
from typing import Optional

import pandas as pd
from pypims import flood
from pypims.IO import InputHipims, Raster

# set some file paths
DATA_PATH = "/data/inputs"  # model input data
OUTPUT_PATH = "/data/outputs"  # model output data


def check_env_var(env_var: str) -> bool:
    """Determines whether an environment variable exists and returns it if so."""
    return getenv(env_var, False)


def get_single_file(path: str, error_if_empty: bool = True) -> Optional[str]:
    """Checks if there is only 1 file in a directory (excluding dotfiles) and
    returns that single file if so.

    Hidden files (i.e. filenames starting with a '.') are ignored.

    Args:
        path: directory to check
        error_if_empty: whether to raise an error if files aren't found.
          Default is True.

    Raises:
        ValueError: if more than one file is found.
        FileNotFoundError: if no file is found when ``error_is_empty`` is True.

    Returns a string path (as pypims doesn't like ``pathlib.Path`` objects
      being passed around internally) if a single file is found, or ``None``
      if the directory is empty and ``error_if_empty`` is False.
    """
    files = [file for file in Path(path).glob("*") if not file.stem.startswith(".")]
    if len(files) > 1:
        raise ValueError(
            "%s does not meet the file specifications. Only 1 file allowed. Found %s",
            str(path),
            files,
        )

    if len(files) == 0:
        if error_if_empty:
            raise FileNotFoundError("No files found within %s", str(path))
        return None
    return str(files[0])


def get_optional_single_file(path: str) -> Optional[str]:
    """Looks for an optional input file, where we may or may not have a volume
    mounted.

    If the volume is mounted, we look for files but don't raise an error if
    they're missing."""
    if Path(path).is_dir():
        return get_single_file(path, error_if_empty=False)
    return None


def set_up_model():
    """Sets up our pypims model so it is ready to run."""

    # pick up environment variables
    ngpus = int(check_env_var("NUM_GPU"))
    simulation_name = check_env_var("SIM_NAME")
    start_time = int(check_env_var("start_time"))
    run_time = int(check_env_var("run_time"))
    output_interval = int(check_env_var("output_interval"))
    backup_interval = int(check_env_var("backup_interval"))

    # set the simulation folder
    case_folder = join(OUTPUT_PATH, f"hipims_case_{simulation_name}")

    # load DEM data
    hipims_input = InputHipims(
        dem_data=get_single_file(join(DATA_PATH, "DEM")),
        num_of_sections=ngpus,
        case_folder=str(case_folder),
    )

    # load (mandatory) rain source
    rain_source = get_single_file(join(DATA_PATH, "rain_source"))
    # load optional rain mask
    if rain_mask := get_optional_single_file(join(DATA_PATH, "rain_mask")):
        rain_mask = Raster(rain_mask).array
    else:
        rain_mask = None
    # set rainfall
    hipims_input.set_rainfall(rain_mask=rain_mask, rain_source=rain_source)

    # load optional landcover data
    if land_cover := get_optional_single_file(join(DATA_PATH, "land_cover")):
        hipims_input.set_landcover(Raster(land_cover))

        # load optional manning coefficients
        if manning_file := get_optional_single_file(join(DATA_PATH, "manning")):
            manning = json.loads(Path(manning_file).read_text())
            hipims_input.set_grid_parameter(
                manning={
                    "param_value": list(manning["manning_lookup"].values()),
                    "land_value": list(map(int, manning["manning_lookup"].keys())),
                    "default_value": manning["default"],
                }
            )
        else:
            print("No manning coefficient data found.")
    else:
        print("No landcover data found. Proceeding without landcover data.")

    # load optional gauge (reporting locations) data
    if gauge := get_optional_single_file(join(DATA_PATH, "gauges")):
        gauges_pos = pd.read_csv(gauge, delimiter=",").values
        hipims_input.set_gauges_position(gauges_pos=gauges_pos)
    else:
        print("No gauge data found. Proceeding without gauge data.")

    # load optional existing water bodies (e.g. river) boundary conditions
    if boundary_file := get_optional_single_file(
        join(DATA_PATH, "waterbody_boundaries")
    ):
        boundaries = json.loads(Path(boundary_file).read_text())["boundaries"]
        for boundary in boundaries:
            boundary.pop("name", None)  # remove human-friendly 'name' field
        hipims_input.set_boundary_condition(boundary_list=boundaries)

    hipims_input.set_initial_condition(
        "h0", 0.0
    )  # hard coded dry start. Initial water depth 0m across catchment.
    hipims_input.set_runtime([start_time, run_time, output_interval, backup_interval])

    return hipims_input


def run(hipims_input):
    """Runs our model and saves a copy of the outputs."""
    case_folder = hipims_input.get_case_folder()

    # Create input files
    hipims_input.write_input_files()
    print(hipims_input)

    # Save a copy of the input object to disk
    hipims_input.save_object(join(case_folder, "input.pickle"))

    # Run HiPIMS
    if hipims_input.num_of_sections > 1:
        flood.run_mgpus(case_folder)  # multi-gpu
    else:
        flood.run(case_folder)


def main():
    run(set_up_model())


main()
